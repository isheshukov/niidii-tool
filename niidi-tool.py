import sys
from Bio import Entrez
import pprint
import click
import pandas as pd
import collections
from itertools import islice, zip_longest
from tqdm import tqdm
import xml.etree.ElementTree as etree

Entrez.email = 'sheshukov.ilya@gmail.com'

#Source: https://docs.python.org/3/library/itertools.html#recipes


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)

@click.group()
def app():
    pass

@app.command()
@click.argument('term')
@click.argument('filename')
def table(**kwargs):
    db = "biosample"
    handle = Entrez.esearch(db=db, term=kwargs['term'], retmax=100000, idtype='acc')
    result = Entrez.read(handle)
    rows = []

    print('There are {} result(s)'.format(len(result['IdList'])))
    proceed = False
    while (not proceed):
        answer = input("Take first N elements ([Number]), all (A), or quit (Q): ")

        proceed = True
        if answer.isnumeric():
            take_first = int(answer)
        elif (answer.lower() == 'a'):
            take_first = len(result['IdList'])
        elif (answer.lower() == 'q'):
            sys.exit(0)
        else:
            print('Wrong input')
            proceed = False

    batch_num = 200
    progress = tqdm(grouper(islice(result['IdList'], take_first), batch_num), total=take_first // batch_num)
    for i in progress:
        handle1 = Entrez.esummary(db=db, id=",".join(list(filter(None.__ne__, i)))
                                  )
        result1 = Entrez.read(handle1)

        samples = result1['DocumentSummarySet']['DocumentSummary']
        for sample in samples:
            row = collections.OrderedDict()
            sample_data = etree.ElementTree(etree.fromstring(sample['SampleData']))


            ids = sample_data.find('Ids')
            for i in range(len(ids)):
                row.update({next(iter(ids[i].attrib.items())): ids[i].text})

            row.update({'date': sample['Date']})
            organism = sample_data.find('Description/Organism')
            for i in range(len(organism)):
                row.update({'organism': organism[i].text})

            attributes = sample_data.find('Attributes')
            for i in range(len(attributes)):
                row.update({attributes[i].attrib['attribute_name']: attributes[i].text})

            rows.append(row)

        handle.close()

    print("Preparing {}...".format(kwargs['filename']))
    table = pd.DataFrame(rows)
    writer = pd.ExcelWriter(kwargs['filename'], engine='openpyxl')
    xl = table.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    print("Done!")

    handle.close()

if __name__ == '__main__':
    app()