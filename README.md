# Install

## Prerequirements 

[Anaconda](https://www.anaconda.com/download/)/[miniconda](https://conda.io/miniconda.html)

## Installing

```
conda env create -f environment.yml
conda activate niidi
python niidi-tool.py
```

# Examples

Making a table 

```
python niidi-tool.py table "Neisseria meningitidis[Organism]" out.xlsx
```